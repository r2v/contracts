import com.google.gson.JsonObject;
import io.yggdrash.core.contract.TransactionReceipt;
import io.yggdrash.core.store.StateStore;
import io.yggdrash.core.store.TransactionReceiptStore;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CoinContractTemplateTest {
    private CoinContractTemplate coinContract = new CoinContractTemplate();

    @Before
    public void setUp() {
        coinContract.init(new StateStore<>(), new TransactionReceiptStore());
    }

    @Test
    public void helloTest() {
        BigDecimal amount = BigDecimal.valueOf(100);
        JsonObject params = new JsonObject();
        params.addProperty("amount", amount);
        TransactionReceipt receipt = coinContract.hello(params);

        assert receipt.isSuccess();
        BigDecimal balance = balanceOf("hello");
        assert balance.equals(BigDecimal.valueOf(100));
    }

    @Test
    public void genesisTest() {
        JsonObject param = new JsonObject();

        JsonObject alloc = new JsonObject();
        param.add("alloc", alloc);

        JsonObject frontier = new JsonObject();
        frontier.addProperty("balance", "1000000000");

        alloc.add("1a0cdead3d1d1dbeef848fef9053b4f0ae06db9e", frontier);

        TransactionReceipt receipt = coinContract.genesis(param);

        assert receipt.isSuccess();

        BigDecimal balance = balanceOf("1a0cdead3d1d1dbeef848fef9053b4f0ae06db9e");
        assert balance.equals(BigDecimal.valueOf(1000000000));

        BigDecimal totalSupply = (BigDecimal)coinContract.query("totalSupply", null);
        assert totalSupply.equals(BigDecimal.valueOf(1000000000));

    }

    private BigDecimal balanceOf(String address) {
        JsonObject param = new JsonObject();
        param.addProperty("address", address);

        return (BigDecimal)coinContract.query("balanceOf", param);
    }
}